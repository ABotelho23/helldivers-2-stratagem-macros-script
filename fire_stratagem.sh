#!/bin/bash

# the name of the stratagem should be the base name of the symlink we're calling the script from
# make sure the symlink starts with s_ and ends with .sh
stratagem="$(basename $0 .sh)"
stratagem_string=''
stratagem_code=''
logfile="/home/${USER}/stratagems.log"

# define possible keys and translations to keybindings
declare -rA keys=(['up']='Up' ['down']='Down' ['left']='Left' ['right']='Right')

#define stratagem key codes
declare -rA stratagems=(
['s_anti_materiel_rifle']='down left right up down'
['s_eagle_110mm_rocket_pods']='up right up left'
['s_eagle_500kg_bomb']='up right down down down'
['s_eagle_airstrike']='up right down right'
['s_eagle_cluster_bomb']='up right down down right'
['s_eagle_napalm_airstrike']='up right down up'
['s_eagle_rearm']='up up left up right'
['s_eagle_strafing_run']='up right right'
['s_expendable_anti_tank']='down down left up right'
['s_gatling_sentry']='down up right left'
['s_guard_dog_rover']='down up left up right right'
['s_heavy_machine_gun']='down left up down down'
['s_machinegun']='down left down up right'
['s_mortar_sentry']='down up right right down'
['s_orbital_380mm_he_barrage']='right down up up left down down'
['s_orbital_gas_strike']='right right down right'
['s_orbital_laser']='right down up right down'
['s_orbital_precision_strike']='right right up'
['s_railgun']='down right down up left right'
['s_reinforce']='up down right left up'
['s_resupply']='down down up right'
['s_seaf_artillery']='right up up down'
['s_sos_beacon']='up down right up'
['s_stalwart']='down left down up up left'
['s_supply_pack']='down left down up up down'
['s_tesla_tower']='down up right up left right'
)

# converts key codes to real keys on the keyboard
assemble_stratagem_string() {
    for key in $stratagem_code; do
        stratagem_string="${stratagem_string} ${keys[${key}]}"
    done
}

# call xdotool to make the keyboard calls using the converted string
stratagem_code="${stratagems[${stratagem}]}"
assemble_stratagem_string
xdotool keydown --delay 500 h keyup h
xdotool key --delay 100 $stratagem_string

echo "${stratagem^^} (${stratagem_code}): $(date)" >> $logfile
