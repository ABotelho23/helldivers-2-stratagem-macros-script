# Helldivers 2 Strategem Macros Script

## Getting started

This script requires ```xdotool``` and ```bash``` version >=5

Please this script wherever you like. Symlink this tool to the stratagems in the form: ```s_orbital_precision_strike.sh``` (#TODO automate the symlink creation)

Call this script via the symlinks, not directly. The script will strip the .sh extension, and find the stratagem code to call via the symlink name. Tested to work with ODeck: https://github.com/willianrod/ODeck

Note that I have special mappings (['up']='v' ['down']='x' ['left']='y' ['right']='w') for my personal rig. You may want to change the keyboard key associated with whatever is up/down/left/right for you. See ```man xdotool```.
